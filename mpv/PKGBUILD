# Maintainer: Christian Hesse <mail@eworm.de>
# Contributor: Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor: Eivind Uggedal <eivind@uggedal.com>

pkgname=mpv
epoch=1
pkgver=0.30.0
pkgrel=4.1
_waf_version=2.0.9
pkgdesc='a free, open source, and cross-platform media player'
arch=('x86_64')
# We link against libraries that are licensed GPLv3 explicitly, libsmbclient
# being one of these. So our package is GPLv3 only as well.
license=('GPL3')
url='https://mpv.io/'
depends=('desktop-file-utils' 'ffmpeg' 'hicolor-icon-theme' 'jack' 'lcms2'
         'libarchive' 'libcaca' 'libcdio-paranoia' 'libdvdnav' 'libgl' 'libva'
         'libxinerama' 'libxkbcommon' 'libxrandr' 'libxss' 'libxv' 'lua52'
         'rubberband' 'smbclient' 'uchardet' 'vulkan-icd-loader' 'wayland'
         'xdg-utils' 'shaderc' 'mujs' 'libplacebo' 'libplacebo.so' 'vapoursynth')
makedepends=('mesa' 'python-docutils' 'ladspa' 'wayland-protocols'
             'ffnvcodec-headers' 'vulkan-headers')
optdepends=('youtube-dl: for video-sharing websites playback')
options=('!emptydirs')
source=("$pkgname-$pkgver.tar.gz::https://github.com/mpv-player/$pkgname/archive/v$pkgver.tar.gz"
        "https://waf.io/waf-${_waf_version}")
sha256sums=('33a1bcb7e74ff17f070e754c15c52228cf44f2cefbfd8f34886ae81df214ca35'
            '2a8e0816f023995e557f79ea8940d322bec18f286917c8f9a6fa2dc3875dfa48')

prepare() {
  cd ${pkgname}-${pkgver}

  install -m0755 "${srcdir}"/waf-${_waf_version} waf
}

build() {
  cd ${pkgname}-${pkgver}

  ./waf configure --prefix=/usr \
    --confdir=/etc/mpv \
    --enable-cdda \
    --enable-dvb \
    --enable-dvdnav \
    --enable-libarchive \
    --enable-libmpv-shared \
    --enable-libsmbclient \
    --enable-vapoursynth

  ./waf build
}

package() {
  cd ${pkgname}-${pkgver}

  ./waf install --destdir="$pkgdir"

  install -m0644 DOCS/{encoding.rst,tech-overview.txt} \
    "$pkgdir"/usr/share/doc/mpv

  install -m0644 TOOLS/lua/* \
    -D -t "$pkgdir"/usr/share/mpv/scripts
}
